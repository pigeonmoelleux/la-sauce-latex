%===========================% Classe sauce %===========================%

%%% Cette classe est destinée à la mise en place du journal `La Sauce`,
%%% journal étudiant de l'École Normale Supérieure de Paris-Saclay.

%%% La présente version est réalisée par Pigeon Moelleux et est disponible
%%% sur l'instance Gitlab du Crans à l'adresse : https://gitlab.crans.org/pigeonmoelleux/la-sauce-latex.

%%% Afin de simplifier la mise en place la création de cette classe, elle a été écrite en LaTeX 3 (voir https://www.latex-project.org/latex3/).
%%% Si vous voulez plus d'explications sur le LaTeX3 utilisé ici, vous pouvez consulter (en anglais) :
%%% * un tutoriel sur LaTeX 3 : https://www.alanshawn.com/latex3-tutorial/ ;
%%% * la documentation du package `xparse` : https://mirror2.sandyriver.net/pub/ctan/macros/latex/contrib/l3packages/xparse.pdf ;
%%% * la documentation des commandes utiliées : https://ctan.math.utah.edu/ctan/tex-archive/macros/latex/contrib/l3kernel/interface3.pdf.

%%% Pour des raisons de compréhension, toutes les commandes créées et tous les commentaires utiles seront en français.
%%% De plus, pour des raisons de conventions, toutes les commandes et tous les commentaires internes seront en anglais.

\NeedsTeXFormat{LaTeX2e}

% LaTeX 3
\RequirePackage{expl3}

\ProvidesExplClass
    {sauce}
    {2023-02-25}
    {0.1.0}
    {Classe pour la Sauce, journal de l'ENS Paris-Saclay}
\LoadClass[12pt, a4paper]{article}

%===========================% Imports %===========================%

% Encodage
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

% Polices
\RequirePackage{fontspec}
\setmainfont[
    Path = ./fonts/,
    Extension = .ttf,
    UprightFont = *-Regular,
    BoldFont = *-Bold,
    ItalicFont = *-Italic,
    BoldItalicFont = *-BoldItalic
]{OpenSans}

\RequirePackage{lettrine}

% Formattage
\RequirePackage{xcolor}
\RequirePackage{ulem}

% Langues
\RequirePackage[main=french,english]{babel}

% Mise en page
\RequirePackage{geometry}
\newgeometry{top=2cm, bottom=2cm, left=2cm, right=2cm}

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{1pt}
\setlength{\headheight}{15pt}
\addtolength{\topmargin}{-15pt}

\RequirePackage{multicol}
\RequirePackage{indentfirst}

% Caractères supplémentaires
\RequirePackage{eurosym}

% Mathématiques
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\RequirePackage{array}

% Figures
\RequirePackage{float}

% Images
\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{caption}

% Tableaux
\RequirePackage{booktabs}
\RequirePackage{multicol}
\RequirePackage{longtable}

% Date
\RequirePackage{datetime2}
\DTMsetup{useregional}
\DTMsavenow{now}

%=======================% Commandes utilisables %=======================%

%%% Générales

% Permet de sauter une ligne
\NewDocumentCommand
    {\jump}
    { O{1} }
    { \vspace{#1\baselineskip} \par }

% Affiche un point médian
\NewDocumentCommand
    {\⋅}
    {}
    { \textperiodcentered }

%%% Pour la une

% Fixe le numéro
\NewDocumentCommand
    {\numero}
    { m }
    { \int_set:Nn \g__sauce_number_int { #1 } }

% Fixe le prix
\NewDocumentCommand
    {\prix}
    { m }
    { \tl_set:Nn \g__sauce_price_tl { #1 } }

% Fixe la température
\NewDocumentCommand
    {\temperature}
    { m }
    { \tl_set:Nn \g__sauce_temp_tl { #1 } }

% Fixe la une
\NewDocumentCommand
    {\une}
    { m }
    { \tl_set:Nn \g__sauce_front_text_tl { #1 } }

% Fixe la citation
\RenewDocumentEnvironment
    {citation}
    { m +!b }
    { 
        \tl_gset:Nn \g__sauce_citation_tl { #2 } 
        \tl_gset:Nn \g__sauce_citation_author_tl { #1 }
    }
    {}

% Fixe l'éditorial
\NewDocumentEnvironment
    { edito }
    { m +!b }
    { 
        \tl_gset:Nn \g__sauce_edito_tl { #2 } 
        \tl_gset:Nn \g__sauce_edito_author_tl { #1 }
    }
    {}

% Modifie le mois
\NewDocumentCommand
    {\mois}
    { m }
    { \tl_set:Nn \g__sauce_month_tl { #1 } }

% Modifie l'année
\NewDocumentCommand
    {\annee}
    { m }
    { \tl_set:Nn \g__sauce_year_tl { #1 } }

% Crée la une
\NewDocumentCommand
    { \makeune }
    {}
    { \_sauce_make_front_page: }

%%% Pour les articles

% Ajoute une signature en fin d'article
\NewDocumentCommand
    {\signature}
    { m }
    {
        \Large
        \begin{flushright}
            {\itshape #1}~
            {\color_select:n {title} $\blacksquare$} 
        \end{flushright}
    }

%%% Couleurs

% Change la couleur des titres
%
% La couleur à donner est sous format hexadécimal.
\NewDocumentCommand
    { \couleurtitre }
    { m }
    { \color_set:nnn { title } { HTML } { #1 } }

% Change le mois

%========================% Machinerie interne %========================%

%%% Variables creation

\int_new:N \g__sauce_number_int
\tl_new:N \g__sauce_price_tl
\tl_new:N \g__sauce_temp_tl
\tl_new:N \g__sauce_front_text_tl
\tl_new:N \g__sauce_citation_tl
\tl_new:N \g__sauce_citation_author_tl
\tl_new:N \g__sauce_edito_tl
\tl_new:N \g__sauce_edito_author_tl

\tl_new:N \g__sauce_month_tl
\tl_set:Nn \g__sauce_month_tl { \DTMfrenchMonthname{\DTMfetchmonth{now}} }

\tl_new:N \g__sauce_year_tl
\tl_set:Nn \g__sauce_year_tl { \DTMfetchyear{now} }

%%% Colors creation

\color_set:nnn { title } { HTML } { D9A621 }

%%% LaTeX variables redefinition
\addto\captionsfrench{
    \renewcommand{\contentsname}{\underline{La~recette~de~cette~Sauce~:}}
}

%%% Pages footer
\fancyfoot[L]{La~Sauce~{ \int_use:N \g__sauce_number_int } }
\fancyfoot[R]{ \_sauce_date_tl: }

%%% LaTeX3 commands definition

\cs_new:Npn \_sauce_date_tl:
{
    {
        \tl_use:N \g__sauce_month_tl 
    }~
    { 
        \tl_use:N \g__sauce_year_tl
    }
}

\cs_new:Npn \_sauce_make_front_page:
{
    \noindent
    {
        % Journal number
        {
            \Huge \bfseries
            \int_compare:nNnTF 
                {\int_use:N \g__sauce_number_int} 
                { = }
                { 0 }
                {}
                {
                    \No {\int_use:N \g__sauce_number_int}
                }
            \quad
        }
        
        % Price
        {
            \large
            \tl_use:N \g__sauce_price_tl
        }
        \hfill
        
        % Month and year
        {
            \Huge
            \_sauce_date_tl:~
        }
    }

    % Temperature
    \begin{flushright}
        \Large
        \tl_use:N \g__sauce_temp_tl
    \end{flushright}

    \jump[3]

    % Main title
    \begin{center}

        % Title
        {
            \fontsize{70}{100}
            \selectfont
            \color_select:n { title }
            \bfseries
            LA~SAUCE
        }

        \jump \par
        % Subtitle
        {
            \fontsize{20}{24}
            \selectfont
            \itshape
            L'information~par~et~pour~les~normalien\⋅ne\⋅s
        }
    \end{center}
    \jump

    % Front text
    \begin{flushright}
        \huge
        \tl_use:N \g__sauce_front_text_tl
    \end{flushright}

    \jump

    % Citation
    {
        \itshape
        \tl_use:N \g__sauce_citation_tl
    }

    % Citation author
    \begin{flushright}
        \LARGE
        \tl_use:N \g__sauce_citation_author_tl
    \end{flushright}

    \jump

    \begin{tabular}{cc}        
        % Editorial
        \begin{minipage}[t]{0.48\textwidth}
            {
                \color_select:n { title }
                \LARGE
                \bfseries
                Édito
            }
            
            \jump

            
            {
                \tl_use:N \g__sauce_edito_tl
            }

            \signature{\tl_use:N \g__sauce_edito_author_tl}
        \end{minipage}

        &

        % Table of contents
        \begin{minipage}[t]{0.48\textwidth}
            \tableofcontents
        \end{minipage}

    \end{tabular}
}