# Classe LaTeX pour La Sauce

## Qu'est-ce que La Sauce ?

Il s'agit du journal de l'ENS Paris-Saclay. Celle-ci est rédigée en LaTeX, et ce dépôt contient une classe LaTeX pour simplifier l'écriture de ce journal.

## Installation

Ce dépôt git contient plusieurs fichiers, les plus importants étant [sauce.cls](sauce.cls) et le dossier [fonts/](fonts/). Pour utiliser la classe LaTeX, vous devez télécharger ces deux éléments, et les placer dans le même dossier.

Vous avez ensuite deux possibilités : écrire votre document dans le même dossier, ou ajouter le dossier à votre `PATH` auquel cas vous pourrez écrire votre document depuis n'importe où sur votre ordinateur.

## Utilisation

Pour utiliser cette classe, il vous suffit d'écrire en préambule de votre document 

```latex
\documentclass{sauce}
```

Pour vous aider, un exemple d'utilisation est disponible dans [exemple.tex] !

### Commandes disponibles

* `\makeune` : Permet de créer directement la première de couverture. Pour cela, vous pouvez définir les paramètres suivants avant la commande `\makeune` pour la compléter :
  * `\numero{<numéro>}` : numéro du journal
  * `\prix{<prix>}` : prix d'un journal
  * `\temperature{<température>}` : température du journal
  * `\une{<Phrase d'accroche>}`
  * `\begin{citation}{<auteur>} <citation> \end{citation}` : citation du numéro
  * `\mois{<mois>}` : modifie le mois défini par défaut
  * `\annee{<annee>}` : modifie l'année définie par défaut

* `\couleurtitre{<couleur hexadécimale>}` : Modifie la couleur des titres (par défaut `D9A621`). La nouvelle couleur doit être donnée sous format hexadécimale (vous pouvez vous aider du site [HTML colors](https://htmlcolors.com/)).

* `\signature{<auteur>}` : Ajoute une signature alignée à droite avec un carré plein de la couleur des titres.